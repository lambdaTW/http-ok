from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='http-ok',
    version='0.1',
    description='The dummy HTTP server always return 200 OK',
    long_description=readme(),
    classifiers=[
        'Programming Language :: Python :: 3.7',
    ],
    keywords='dummy fake mock http ok 200',
    url='https://gitlab.com/lambdaTW/http-ok',
    author='lambdaTW',
    author_email='lambda@lambda.tw',
    license='MIT',
    packages=['http_ok'],
    install_requires=[
        'flask',
    ],
    entry_points = {
        'console_scripts': ['ok=http_ok.run:main'],
    },
    zip_safe=False
)
